
mat = [1, 2, 3; ...
       4, 5, 6;
       7, 8, 9];
x = [1, 2, 3];
y = mat*x';

addpath('../lib')
libname = 'mylib';
header_name = 'mylib.h';
loadlibrary(libname, header_name)
funcname = 'matvec_colmajor';

[Nrow, Ncol] = size(mat);
yfort = zeros(Nrow, 1);

[mat, x, yfort] = calllib(libname, funcname, mat, x, int32(Nrow), int32(Ncol), yfort);

diff = sum(abs(yfort - y));
disp(sprintf('diff: %f', diff));

