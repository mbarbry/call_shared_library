using LinearAlgebra
using Base.Libc.Libdl

mat = [1.0 2.0 3.0;
       4.0 5.0 6.0;
       7.0 8.0 9.0]
x = [1.0, 2.0, 3.0]

yref = mat*x
println(yref)

push!(Libdl.DL_LOAD_PATH, "../lib/")

Nrow, Ncol = size(mat)
yfort = zeros(Float64, Nrow)

ccall((:matvec_colmajor,"mylib"), Cvoid,
      (Ptr{Float64}, Ptr{Float64}, Int, Int, Ptr{Float64}),
      mat, x, Nrow, Ncol, yfort)
diff = abs.(sum(yref - yfort))
println("diff: ", diff)

