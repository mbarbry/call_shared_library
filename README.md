# Example Fortran library

An easy way to speed up and protect sensitive code is to create a C or Fortran
library that can be called from Python, Matlab or Julia.

In this repository, we demonstrate how to call some fortran library from Python,
Matlab and Julia

## How to compile the library

    cd lib
    make

## Run an example

    cd examples
    python mymatvec.py
    julia mymatvec.jl
    matlab -nodisplay -nosplash -r "run('mymatvec.m');exit;"
