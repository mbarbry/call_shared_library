# The Fortran lib

The Fortran code can easily be compiled into a shared library via the provided
makefile.

To add more Fortran code, just add the `*.o` filename to the `OBJECT` variable.

The file `mylib.h` is required for matlab compatibility.
