void hello_simple();
void hello(int Ncol);
void matvec_rowmajor(double *matrix, double *vec_in, int Ncol, int Nrow, double *vec_out);
void matvec_colmajor(double *matrix, double *vec_in, int Ncol, int Nrow, double *vec_out);
