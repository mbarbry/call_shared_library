import numpy as np
import ctypes as ct

def matvec_py(mat, vec):

    vec_out = np.zeros((mat.shape[0]), dtype=mat.dtype)

    for i in range(mat.shape[0]):
        for j in range(mat.shape[1]):

            vec_out[i] += mat[i, j]*vec[j]

    return vec_out

libmatvec = ct.CDLL("../lib/mylib.so")

mat = np.array([[1, 2, 3],
                [4, 5, 6],
                [7, 8, 9]], dtype=np.float64)
x = np.array([1, 2, 3], dtype=np.float64)

yref = mat.dot(x)
ypy = matvec_py(mat, x)

yfort = np.zeros((mat.shape[0]), dtype=mat.dtype)

libmatvec.hello(ct.c_int(3))
libmatvec.matvec_rowmajor(mat.ctypes.data_as(ct.POINTER(ct.c_double)),
                          x.ctypes.data_as(ct.POINTER(ct.c_double)),
                          ct.c_int(mat.shape[0]),
                          ct.c_int(mat.shape[1]),
                          yfort.ctypes.data_as(ct.POINTER(ct.c_double)))

print(ypy)
print("diff python: ", np.sum(abs(yref - ypy)))
print("diff fortran: ", np.sum(abs(yref - yfort)))

