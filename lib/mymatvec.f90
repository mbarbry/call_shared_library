module mymatvec

  use iso_c_binding, only: c_double, c_int

  contains

!
!
!
subroutine hello_simple() bind(c, name='hello_simple')

  implicit none

  print*, "hello simple"

end subroutine ! hello_simple

!
!
!
subroutine hello(Ncol) bind(c, name='hello')

  implicit none
  integer(c_int), intent(in), value :: Ncol

  print*, "hello: ", Ncol

end subroutine ! hello


!
!
!
subroutine matvec_rowmajor(matrix, vec_in, Ncol, Nrow, vec_out) bind(c, name='matvec_rowmajor')

  implicit none
  integer(c_int), intent(in), value :: Ncol, Nrow
  real(c_double), intent(in) :: matrix(Ncol*Nrow), vec_in(Ncol)
  real(c_double), intent(inout) :: vec_out(Nrow)

  ! internal variables
  integer(c_int) :: i, j

  ! One must be careful to the array memory order
  ! by default it is column major in Fortran, but array passed from Python
  ! are in row major while array from julia are in column major.
  do i = 1, Nrow
    do j = 1, Ncol
      vec_out(i) = vec_out(i) + matrix((i-1)*Ncol + j)*vec_in(j)
    enddo
  enddo

end subroutine ! matvec

!
!
!
subroutine matvec_colmajor(matrix, vec_in, Ncol, Nrow, vec_out) bind(c, name='matvec_colmajor')

  implicit none
  integer(c_int), intent(in), value :: Ncol, Nrow
  real(c_double), intent(in) :: matrix(Ncol*Nrow), vec_in(Ncol)
  real(c_double), intent(inout) :: vec_out(Nrow)

  ! internal variables
  integer(c_int) :: i, j

  ! One must be careful to the array memory order
  ! by default it is column major in Fortran, but array passed from Python
  ! are in row major while array from Julia are in column major.
  do j = 1, Ncol
    do i = 1, Nrow
      vec_out(i) = vec_out(i) + matrix((j-1)*Nrow + i)*vec_in(j)
    enddo
  enddo

end subroutine ! matvec


end module ! mymatvec
